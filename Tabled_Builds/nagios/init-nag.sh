#!/bin/bash

establish-conf() {
    if [ -f /staging/nagios.cfg ]; then
	printf "[INFO] Custom nagios.cfg detected, linking.."
	ln -s /staging/nagios.cfg /etc/nagios/nagios.cfg
    fi

    if [ -f /staging/cgi.cfg ]; then
	printf "[INFO] Custom cgi.cfg detected, linking.."
	ln -s /staging/cgi.cfg /etc/nagios/cgi.cfg
    fi

    if [ -f /staging/resources.cfg ]; then
	printf "[INFO] Custom resources.cfg detected, linking.."
	ln -s /staging/resources.cfg /etc/nagios/resources.cfg
    fi

    if [ -d /staging/objects ]; then
	printf "[INFO] Custom objects directory detected, linking.."
	ln -s /staging/objects /etc/nagios/objects
    fi
}

init-web-server() {
    lighttpd -f /etc/lighttpd/lighttpd.conf
}

init-nagios() {
    nagios -v /etc/nagios/nagios.cfg
    nagios -d /etc/nagios/nagios.cfg
}

stay-alive(){
    while [ -f /usr/local/bin/init-nag.sh ]; do
	sleep 86400
    done
}

establish-conf
init-web-server
init-nagios
stay-alive
