#!/bin/bash
# Launch Cage inside of Wayland Containers

main(){
    chmod +x /kiosk/$PROGRAM
    /usr/bin/cage /kiosk/$PROGRAM
}

while getopts ":p:" opt; do
    case $opt in
	p) PROGRAM=$OPTARG ;;
    esac
done

main
