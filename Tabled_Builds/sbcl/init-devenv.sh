#!/bin/bash

if [[ $1 == "" ]]; then
    /usr/bin/sbcl
fi

while getopts "e:l:" opt; do
    case $opt in
	e) /usr/bin/sbcl --eval $OPTARG ;;
	l) /usr/bin/sbcl --load $OPTARG ;;
    esac
done
