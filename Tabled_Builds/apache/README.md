# Usage:

### To Build
Issue docker build -t ws-apache . in the project directory, or a similar more granular command if required

### Verify Functionality
Running the container with the -v flag will display a verification site setup from a custom httpd.conf file.

docker run ws-apache -v

### Loading a Custom Site
Ensure the name of your primary page is index.html
touch Stage_Me to the path containing your html files

Finally run docker run -v /path/to/site/files:/var/wwww/site ws-apache -l to launch the container which will copy the files in the given path to the /var/www/site path and launch apache.

#### Note
This is a fairly cludgey way to launch a custom site, I don't intend this container to be used for actual site/application hosting, simply as a way to test simple websites in apache.