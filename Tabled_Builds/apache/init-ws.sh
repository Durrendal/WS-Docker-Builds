#!/bin/bash
# Author: Will Sinatra <wpsinatra@gmail.com>
# Create a basic webserver container

verify-site (){
    if [ ! -f /var/www/site_loaded ]; then
	echo "Site doesn't exist, placing temporary verification html"
	cp /tmp/httpd.conf /etc/apache2/httpd.conf
	cp /tmp/verification.html /var/www/index.html

	load-site
    else
	load-site
    fi
}

configure-site (){
    cp /var/www/site/httpd.conf /etc/apache2/httpd.conf
    load-site
    }

load-site (){
    if [ -f /var/www/site/Stage_Me ]; then
	echo "Loading Site Config"
	cp -r /var/www/site/* /var/www/localhost/htdocs/
    fi
    
    echo "Starting Server"
    httpd -D FOREGROUND -f /etc/apache2/httpd.conf
}

usage (){
    printf "The docker image requires arguments, they are as followed:

-v: Verify site installation, stages a temporary verification site and httpd.conf
-c: Configure site
-l: Load Configured Site
"
}

#if [[ $1 -eq 0 ]]; then
#    usage ;
#else
    while getopts "vlc" opt; do
	case $opt in
	    v) verify-site ;;
	    c) configure-site ;;
	    l) load-site ;;
	esac
    done
#fi
