# Building:

Docker build -t next .

# Usage:

This is currently a work in progress, as such we're using insecure xhost settings below, just so you're aware.

Allow x to accept the container connection:
xhost +

Run the container, with host network, privileged, and bound to the host X server:
docker run -it --net=host --privileged -v /tmp/.X11-unix/:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY next

# Issues:

webkit2gtk doesn't appear to render correctly, the contiainerized next will process keystrokes, but it doesn't display anything in the window it creates.