#!/bin/bash

if [ ! -d /var/run/dbus ]; then
    mkdir -p /var/run/dbus
fi

dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address

export NO_AT_BRIDGE=1

dbus-launch /usr/bin/next
