#!/bin/bash
# KlockWork Systems, LLC. 2019
# Author: Will Sinatra <will.sinatra@klockwork.net>
# Initialize VPN inside of Alpine container

if [ -f "/kws-client/configured" ]; then
    echo "Server configured.. starting.."
    /usr/sbin/openvpn --config /etc/openvpn/server.conf
else
    echo "No server configured.. configuring.."
    cd /scripts
    expect ersa-expect $1 $2 $3 $4 $5 $6
    touch /kws-client/configured
    echo "Server configured.. starting.."
    /usr/sbin/openvpn --config /etc/openvpn/server.conf
fi

while [ -f "/kws-client/server.conf" ]; do
    sleep 1d
done
