#!/bin/bash
# KlockWork Systems, LLC. 2019
# Author: Will Sinatra <will.sinatra@klockwork.net>
# Provision client configuration from provided info
CLIENT=$1

gen_cert (){
    cd /usr/share/easy-rsa
    
    #Configure PKI and build CA
    ./easyrsa init-pki
    sed -i '3d' /usr/share/easy-rsa/pki/openssl-easyrsa.cnf
    ./easyrsa build-ca nopass

    # Copy created cert to server root
    cp /usr/share/easy-rsa/pki/ca.crt /etc/openvpn/server
    cp /usr/share/easy-rsa/pki/ca.crt /kws-client/ca.crt
    mkdir /usr/share/easy-rsa/keys
    cp /usr/share/easy-rsa/pki/ca.crt /usr/share/easy-rsa/keys/ca.crt

    echo "Passed CA"

    # Gen DH Params
    ./easyrsa gen-dh
    cp /usr/share/easy-rsa/pki/dh.pem /usr/share/easy-rsa/keys/dh1024.pem
    cp /usr/share/easy-rsa/pki/dh.pem /kws-client/dh1024.pem

    echo "End of DH"

    # Build Server Keys & Stage
    ./easyrsa build-server-full $CLIENT-server nopass
    cp /usr/share/easy-rsa/pki/private/$CLIENT-server.key /etc/openvpn/
    cp /usr/share/easy-rsa/pki/private/$CLIENT-server.key /kws-client/$CLIENT-server.key
    cp /usr/share/easy-rsa/pki/private/$CLIENT-server.key /usr/share/easy-rsa/keys/$CLIENT-server.key
    cp /usr/share/easy-rsa/pki/issued/$CLIENT-server.crt /usr/share/easy-rsa/keys/$CLIENT-server.crt

    echo "Server Keys Copied"

    # Build Client Keys & Stage
    ./easyrsa build-client-full $CLIENT nopass
    cp /usr/share/easy-rsa/pki/private/$CLIENT.key /kws-client/$CLIENT.key
    cp /usr/share/easy-rsa/pki/issued/$CLIENT.crt /kws-client/$CLIENT.crt
    cp /usr/share/easy-rsa/pki/private/$CLIENT.key /usr/share/easy-rsa/keys/$CLIENT.key

    echo "EasyRSA Complete."
}

gen_cert
