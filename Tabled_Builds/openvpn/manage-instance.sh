#!/bin/bash
# KlockWork Systems, LLC. 2019
# Author: Will Sinatra <will.sinatra@klockwork.net>
# Manage running VPN container

genkey (){
    cd /usr/share/easy-rsa
    ./easyrsa gen-req $CLIENT-req nopass
    cp pki/reqs/$CLIENT-req.req /kws-client/$CLIENT-req.req
    cp pki/private/$CLIENT-req.key /kws-client/$CLIENT-req.key
    echo "Genkey Complete"
}

revkey (){
    cd /usr/share/easy-rsa
    ./easyrsa revoke-full $CLIENT-req

    echo "Revoke Complete"
}

while getopts "g:r:c:" opt; do
    case $opt in
	g) genkey ;;
	r) revkey ;;
	c) CLIENT=$OPTARG ;;
    esac
done
