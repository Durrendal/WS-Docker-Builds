#!/bin/bash
# KlockWork Systems, LLC 2019
# Author: Will Sinatra <will.sinatra@klockwork.net>

gen_ovpn_conf (){
    sed -i 's/PUBIP/'$PIP'/g' /tmp/openvpn.conf
    sed -i 's/VPNPORT/'$VPOR'/g' /tmp/openvpn.conf
    sed -i 's/CLIENTSERVER/'$CSER'/g' /tmp/openvpn.conf
    sed -i 's/LANIP/'$LIP'/g' /tmp/openvpn.conf
    sed -i 's/KEEPER/'$KEEP'/g' /tmp/openvpn.conf

    cp /tmp/openvpn.conf /etc/openvpn/server.conf
    cp /tmp/openvpn.conf /kws-client/server.conf

    echo "OVPN Conf Complete."
}

gen_client_conf (){
    sed -i 's/PUBIP/'$PIP'/g' /tmp/client.conf
    sed -i 's/VPNPORT/'$VPOR'/g' /tmp/client.conf
    sed -i 's/CLIENTSERVER/'$CSER'/g' /tmp/client.conf
    sed -i 's/CLIENT/'$CLIENT'/g' /tmp/client.conf

    cp /tmp/client.conf /kws-client/$CLIENT.conf
    
    echo "Client Conf Complete."
}

ensure_tun (){
    
    mkdir -p /dev/net
    mknod /dev/net/tun c 10 200
    chmod 666 /dev/net/tun

    echo "Tun Provisioned"
}

while getopts "a:b:c:d:e:f:" opt; do
    case $opt in
	a) PIP=$OPTARG ;;
	b) VPOR=$OPTARG ;;
	c) CSER=$OPTARG ;;
	d) LIP=$OPTARG ;;
	e) KEEP=$OPTARG ;;
	f) CLIENT=$OPTARG ;;
    esac
done

gen_ovpn_conf
gen_client_conf
ensure_tun
