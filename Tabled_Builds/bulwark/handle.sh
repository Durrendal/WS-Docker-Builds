#!/bin/ash

cp /var/bulwark/authorized_keys /home/bulwark/.ssh/authorized_keys
chmod 0640 /home/bulwark/.ssh/authorized_keys
chown bulwark:bulwark /home/bulwark/.ssh/authorized_keys

/usr/sbin/sshd -D -e -f /var/bulwark/ssh/sshd_config -E /var/bulwark/log/messages &
fail2ban-client -c /var/bulwark/fail2ban/ -x -f -p /var/run/fail2ban/fail2ban.pid --logtarget /var/bulwark/log/fail2ban.log --loglevel 3 -v start
