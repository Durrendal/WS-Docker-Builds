## Usage:

On the host, ensure you have a directory container an ingest & accumulate directory. These need to be readable and writeable by the container.

Run the container with the following arguments.
docker run -v /path/on/host/:/var/devorabilis devorabilis

Any files places in ingest will be devoured, and accumulated as mpg files in the accumulate directory.