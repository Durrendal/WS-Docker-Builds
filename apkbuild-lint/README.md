# What?
An Alpine Linux docker container that lints APKBUILDs. Base off of the official APKBUILD linter used in Alpine Linux's CI/CD on aports.

## Overview:
![apkbuild-lint overview](img/apkbuild-lint.jpg)

### Example Output:
```
Running with gitlab-runner 12.9.0 (4c96e5ad)
  on alpine-packages LheYdtfQ
Preparing the "docker" executor
00:01
Using Docker executor with image durrendal/apkbuild-lint:latest ...
Pulling docker image durrendal/apkbuild-lint:latest ...
Using docker image sha256:7bb1cb3e0e74e68696b78f0b56e6fddfdb2f3f09dbb769b501c0acf84a9373c0 for durrendal/apkbuild-lint:latest ...
Preparing environment
00:01
Running on runner-LheYdtfQ-project-13270553-concurrent-0 via Gitlab-CI-Runner...
Getting source from Git repository
00:02
Fetching changes...
Reinitialized existing Git repository in /builds/Durrendal/WS-Alpine-Packages/.git/
From https://gitlab.com/Durrendal/WS-Alpine-Packages
 * [new ref]         refs/pipelines/171866055 -> refs/pipelines/171866055
   f8df564..5292238  master                   -> origin/master
Checking out 5292238c as master...
Skipping Git submodules setup
Restoring cache
00:01
Downloading artifacts
00:01
Running before_script and script
00:02
$ git fetch https://gitlab.com/Durrendal/WS-Alpine-Packages.git +refs/heads/master:refs/heads/master
From https://gitlab.com/Durrendal/WS-Alpine-Packages
   f8df564..5292238  master     -> master
$ git diff --name-only $(git log --pretty=oneline | head -n1 | awk '{print $1}') $(git log --pretty=oneline | head -n2  | tail -n 1 | awk '{print $1}') | xargs -n1 -r dirname | uniq | lint
==> Linting testing/kismet
======================================================
                abuild sanitycheck:
======================================================
>>> kismet: Checking sanity of /builds/Durrendal/WS-Alpine-Packages/testing/kismet/APKBUILD...
>>> WARNING: kismet: "GPL" is not a known license
>>> WARNING: kismet: Please use valid SPDX license identifiers found at: https://spdx.org/licenses
======================================================
                apkbuild-shellcheck:
======================================================
======================================================
                  apkbuild-lint:
======================================================
IC:[AL7]:APKBUILD:23:indent with tabs
IC:[AL7]:APKBUILD:24:indent with tabs
IC:[AL7]:APKBUILD:25:indent with tabs
IC:[AL7]:APKBUILD:26:indent with tabs
IC:[AL7]:APKBUILD:27:indent with tabs
IC:[AL7]:APKBUILD:31:indent with tabs
IC:[AL7]:APKBUILD:33:indent with tabs
IC:[AL7]:APKBUILD:34:indent with tabs
IC:[AL7]:APKBUILD:35:indent with tabs
IC:[AL7]:APKBUILD:36:indent with tabs
IC:[AL7]:APKBUILD:38:indent with tabs
MC:[AL29]:APKBUILD:13:$pkgname should not be used in the source url
SC:[AL54]:APKBUILD:18:prepare() is missing call to 'default_prepare'
Running after_script
00:01
Uploading artifacts for failed job
00:02
ERROR: Job failed: exit code 1
```

## Configuring:
Inside of your gitlab-ci.yml file add the following replacing $variables:

```
stages:
  - lint

variables:
  GIT_STRATEGY: fetch
  GIT_DEPTH: 0
  REPO_PATH: /builds/$GITUSERNAME/$REPONAME

default:
  before_script:
  - >-
    git fetch $GITURL
	+refs/heads/master:refs/heads/master

lint:
  stage: lint
  image: durrendal/apkbuild-lint:latest
  script:
    - git diff --name-only $(git log --pretty=oneline | head -n1 | awk '{print $1}') $(git log --pretty=oneline | head -n2  | tail -n 1 | awk '{print $1}') | xargs -n1 -r dirname | uniq | lint   allow_failure: true
  tags:
    - alpine
```

## Usage:
Inside of your aports repo simply git push commits containing APKBUILDS. The linter will run on each commit. If the file is not an APKBUILD, the linter will throw warnings saying as much.