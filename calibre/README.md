# What?
A Debian bullseye based Calibre container, which can be pointed a volume and mounted into the /library directory

# Overview:
![calibre docker](img/calibredocker-map.jpg)

## Building:
```
docker build -t calibre .
```

## Usage:
```
docker run -p 8080:80 -v /library:/library calibre
```