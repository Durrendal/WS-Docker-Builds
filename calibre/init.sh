#!/bin/bash

/usr/bin/calibredb add /library/*.epub /library/*.mobi /library/*.pdf --with-library /library/
/usr/bin/calibre-server library
