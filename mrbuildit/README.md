# What?
```
Let mrbuildit build it!!
```

Alpine Linux APKBUILD containers for the following architectures:

* x86_64
* x86
* aarch64
* armv7
* armhf

Each container runs abuild -r on a provided APKBUILD repo, pulled from a git head fetch.

# Overview:
![mrbuildit](img/mrbuildit.jpg)

## Configuration:
gitlab-ci configuration coming soon..

## Usage:
Coming soon again..

## Example Output:
Coming Soon..