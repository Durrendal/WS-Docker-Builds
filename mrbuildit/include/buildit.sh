#!/bin/sh
cd $REPO_PATH

COMMIT_PATH=$(git diff --name-only $(git log --pretty=oneline | head -n1 | awk '{print $1}') $(git log --pretty=oneline | head -n2  | tail -n 1 | awk '{print $1}') | xargs -r -n1 dirname | uniq)

verify_commit() {	
    if [[ $COMMIT_PATH == . ]]; then
	echo "Commit is a dotfile"
	exit 0
    fi
    
    if [[ -d $COMMIT_PATH ]]; then
	echo "Commit Path is a directory"
	build
    elif [[ -f $COMMIT_PATH ]]; then
	echo "Commit Path is not a directory"
	exit 1
    else
	echo "Commit Path is neither a directory nor a file"
	exit 1
    fi
}

build() {
    printf "Base Branch: "$REPO_PATH"\nCommit Path: "$COMMIT_PATH"\n"

    sudo sed -i 's/JOBS=[0-9]*/JOBS=$(nproc)/' /etc/abuild.conf
    sudo apk -U upgrade -a
    
    cd $REPO_PATH$COMMIT_PATH
    abuild checksum
    abuild -r
}

verify_commit
