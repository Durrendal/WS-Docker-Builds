#!/bin/sh

build() {
    printf "Commencing Localized Build.."
    cd /tmp

    sudo sed -i 's/JOBS=[0-9]*/JOBS=$(nproc)/' /etc/abuild.conf
    sudo apk -U upgrade -a
    
    abuild checksum
    abuild -r
}

build
