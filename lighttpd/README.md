# What?
An Alpine Lighttpd server that takes the contents of a volume and a lighttpd configuration, and establishes a site inside of the container. Unlike most docker containers that use the files on the host, this container migrats them into /tmp/site to serve them, this isn't really necessary, and it results in a lot of system bloat. Lighty was one of my first containers, and I'm kind of keeping it for the sake of nostalgia, in reality it's a pretty poorly constructed lighttpd container, and is really only useful for testing.

# Overview:
![lighty overview](img/lighty-docker.jpg)

## Build:
```
docker build -t lighty .
```

## Usage:
```
docker run -v /path/on/host:/tmp/site -p 80:80 -p 443:443 lighty
```