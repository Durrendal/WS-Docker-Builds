#!/bin/bash
# Author: Will Sinatra <wpsinatra@gmail.com>
# Initialize lighttpd webserver

check_for_configuration(){
    if [ -f /tmp/site/lighttpd.conf ]; then
	echo "[OK]  Site configuration exists, staging"
	cp /tmp/site/lighttpd.conf /etc/lighttpd/lighttpd.conf
    else
	echo "[ERR]  lighttpd.conf doesn't exist in provided volume."
	exit
    fi
}

test_for_configuration_errors(){
    if [[ $(lighttpd -t -f /etc/lighttpd/lighttpd.conf) = "Syntax OK" ]]; then
	echo "[OK]  Lighttpd configuration valid, continuing."
	cp /tmp/site/lighttpd.conf /etc/lighttpd/lighttpd.conf
    else
	echo "[ERR]  Error in lighttpd.conf file provided."
       	exit
    fi
}

set_the_stage(){
    SiteStage=$(grep var.basedir /tmp/site/lighttpd.conf | head -n1 | cut -d " " -f 4 | sed 's/"//g')
    
    mkdir $SiteStage
    
    shopt -s extglob
    rsync -avr --exclude="/tmp/site/lighttpd.conf" /tmp/site/* $SiteStage

    find $SiteStage -type d -exec chmod 0755 {} \;
    find $SiteStage -type f -exec chmod 0644 {} \;
    find $SiteStage -type f -name '*.php' -exec chmod 600 {} \;
    echo "[OK]  Site staged, starting webserver!"
}

run_webserver(){
       
    lighttpd -D -f /etc/lighttpd/lighttpd.conf
}

check_for_configuration
test_for_configuration_errors
set_the_stage
run_webserver
