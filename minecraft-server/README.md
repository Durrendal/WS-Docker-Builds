# What?
A dockerized minecraft server, built into an Alpine edge instance

# Container Overview:
![Minedock Overview](img/minedock-docker.jpg)

# Use
```
docker run -it -p 25565:25565 -v /minecraft:/minecraft durrendal/minedock:latest -l 500M -u 1000M
```

if the directory provided to minedock is empty, it will populate enough base configuration to run the container. This will be version 1.16.1 until I add additional parsing logic.

-l passes the lower memory bound
-u passes the upper memory bound

this way you can more strictly control the resources used by the server without using a full blown docker compose yaml