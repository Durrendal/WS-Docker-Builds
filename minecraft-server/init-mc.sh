#!/bin/bash

main (){
    echo "Initializing server.."

    if [ ! -f /minecraft/eula.txt ]; then
		echo "Eula doesn't exist, staging.."
		mv /var/eula.txt /minecraft/
    else
		echo "Eula exists.."
    fi
    
    if [ ! -f /minecraft/server.properties ]; then
		echo "Properties doesn't exist, staging.."
		mv /var/server.properties /minecraft/
    else
		echo "Properties exist.."
    fi
    
    if [ ! -f /minecraft/server.jar ]; then
		echo "Server doesn't exist.."
		#1.17 Release
		wget https://launcher.mojang.com/v1/objects/0a269b5f2c5b93b1712d0f5dc43b6182b9ab254e/server.jar -O /minecraft/server.jar
    else
		echo "Server exists.."
    fi
    
    cd /minecraft
    java -Xms$LOWER -Xmx$UPPER -jar /minecraft/server.jar nogui
}

while getopts "l:u:" opt; do
    case $opt in
		l) LOWER=$OPTARG ;;
		u) UPPER=$OPTARG ;;
    esac
done

main
