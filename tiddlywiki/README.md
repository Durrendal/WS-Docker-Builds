# What?
A Tiddlywiki instance in an Alpine container, pass it a volume filled with tiddlers, or an empty directory to persist new ones.

# Overview:
![tiddly docker](img/tiddlydocker-map.jpg)

## Building:
```
docker build -t tiddlydocker .
```

## Usage:
```
docker run -p 8080:80 -v /tiddlers:/tiddlydocker/tiddlers tiddlydocker
```