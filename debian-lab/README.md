# What?
A very simple Debian linux image with a couple of tools for setting up quick lab environments. Use it as a base, or hack away at it as is. For me this is the perfect ad hoc "how does X work" container, which prevents me from litering my workstations with packages and other cruft.

## Overview:
![lab overview](img/lab-docker.jpg)

## Tools:
* mg
* htop
* bmon
* tmux

## Building:
```
docker build -t lab .
```

## Usage:
```
docker run -it -v /path/on/host:/lab -p 80:80 -p 52517:52517 lab
```