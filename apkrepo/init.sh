#!/bin/ash

find /repo -type f -exec chmod 0644 {} \;
find /repo -type d -exec chmod 0755 {} \;
/usr/sbin/lighttpd -D FOREGROUND
