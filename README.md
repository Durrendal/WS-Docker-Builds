# Docker Container Builds

A collection of docker builds I've created to fit various use cases, either professional or personal.

## Tabled Builds:
Anything found in Tabled_Builds can be considered abandoned. I do not maintain them, cannot guarantee they work in any fashion, or even build. These are the culmination of experiments and past projects that no longer serve a purpose to maintain.