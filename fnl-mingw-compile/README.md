# What?
fnl-mingw-compile is a Debian based docker container complete with all of the libraries needed to compile Windows executables from Fennel source code. This was built to address the incomplete state of mingw packaging on Alpine Linux in a time efficient manner. Executables produced this way run without issue on Windows 10.

# Overview:
![fnl-mingw-compile](img/fnl-mingw-compile.jpg)

## Build:
```
docker build -t fnl-mingw-compile .
```

## Usage:
fnl-mingw-compile must be called from inside a source directory, and said source directory must contain a build.sh file defining your compiliation methodology.

```
cd /path/to/source-code

docker run -it -v $(pwd)/:/buildstuff durrendal/fnl-mingw-compile:latest
```

## Example Build.sh
The following build.sh file sources lua5.3, compiles it with mingw 32bit to produce a .a library, and then compiles source-code.fnl to a 32bit Windows binary.

```
#!/bin/bash

#Pull in Lua 5.3.5 source code
curl https://www.lua.org/ftp/lua-5.3.5.tar.gz | tar xz

#Compile Lua5.3.5 for 32bit Win
make -C lua-5.3.5 mingw CC=i686-w64-mingw32-gcc

#Compile fa with 32bit Win C Compiler
CC=i686-w64-mingw32-gcc fennel --compile-binary fa.fnl fa lua-5.3.5/src/liblua.a /usr/include/lua5.3

#Clean up build deps
sudo rm -r lua-5.3.5  
```

### Example Output:
```
src|>> docker run -it -v $(pwd)/:/buildstuff fnl-mingw-compile
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  296k  100  296k    0     0   163k      0  0:00:01  0:00:01 --:--:--  163k
make: Entering directory '/buildstuff/lua-5.3.5'
cd src && make mingw
make[1]: Entering directory '/buildstuff/lua-5.3.5/src'
make "LUA_A=lua53.dll" "LUA_T=lua.exe" \
"AR=i686-w64-mingw32-gcc -shared -o" "RANLIB=strip --strip-unneeded" \
"SYSCFLAGS=-DLUA_BUILD_AS_DLL" "SYSLIBS=" "SYSLDFLAGS=-s" lua.exe
make[2]: Entering directory '/buildstuff/lua-5.3.5/src'
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lua.o lua.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lapi.o lapi.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lcode.o lcode.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lctype.o lctype.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o ldebug.o ldebug.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o ldo.o ldo.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o ldump.o ldump.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lfunc.o lfunc.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lgc.o lgc.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o llex.o llex.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lmem.o lmem.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lobject.o lobject.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lopcodes.o lopcodes.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lparser.o lparser.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lstate.o lstate.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lstring.o lstring.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o ltable.o ltable.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o ltm.o ltm.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lundump.o lundump.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lvm.o lvm.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lzio.o lzio.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lauxlib.o lauxlib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lbaselib.o lbaselib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lbitlib.o lbitlib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lcorolib.o lcorolib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o ldblib.o ldblib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o liolib.o liolib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lmathlib.o lmathlib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o loslib.o loslib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lstrlib.o lstrlib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o ltablib.o ltablib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o lutf8lib.o lutf8lib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o loadlib.o loadlib.c
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_BUILD_AS_DLL    -c -o linit.o linit.c
i686-w64-mingw32-gcc -shared -o lua53.dll lapi.o lcode.o lctype.o ldebug.o ldo.o ldump.o lfunc.o lgc.o llex.o lmem.o lobject.o lopcodes.o lparser.o lstate.o lstring.o ltable.o ltm.o lundump.o lvm.o lzio.o lauxlib.o lbaselib.o lbitlib.o lcorolib.o ldblib.o liolib.o lmathlib.o loslib.o lstrlib.o ltablib.o lutf8lib.o loadlib.o linit.o 
strip --strip-unneeded lua53.dll
i686-w64-mingw32-gcc -o lua.exe -s  lua.o lua53.dll -lm  
make[2]: Leaving directory '/buildstuff/lua-5.3.5/src'
make "LUAC_T=luac.exe" luac.exe
make[2]: Entering directory '/buildstuff/lua-5.3.5/src'
i686-w64-mingw32-gcc -O2 -Wall -Wextra -DLUA_COMPAT_5_2     -c -o luac.o luac.c
ar rcu liblua.a lapi.o lcode.o lctype.o ldebug.o ldo.o ldump.o lfunc.o lgc.o llex.o lmem.o lobject.o lopcodes.o lparser.o lstate.o lstring.o ltable.o ltm.o lundump.o lvm.o lzio.o lauxlib.o lbaselib.o lbitlib.o lcorolib.o ldblib.o liolib.o lmathlib.o loslib.o lstrlib.o ltablib.o lutf8lib.o loadlib.o linit.o 
ar: `u' modifier ignored since `D' is the default (see `U')
ranlib liblua.a
i686-w64-mingw32-gcc -o luac.exe   luac.o liblua.a -lm  
make[2]: Leaving directory '/buildstuff/lua-5.3.5/src'
make[1]: Leaving directory '/buildstuff/lua-5.3.5/src'
make: Leaving directory '/buildstuff/lua-5.3.5'
```

And the resultant executable file appears at the end in the source directory, all you need to do is export it to a Windows computer to test things out.

```
src|>> ls -al
total 872
drwxrwsr-x 2 wsinatra wsinatra   4096 Jul 29 23:34 .
drwxr-sr-x 5 wsinatra wsinatra   4096 Jul 23 16:03 ..
-rwxrwxr-x 1 wsinatra wsinatra   3268 Jul 23 16:03 Makefile
-rwxrwxr-x 1 wsinatra wsinatra    363 Jul 29 23:31 build.sh
-rwxr-xr-x 1 wsinatra wsinatra 843411 Jul 29 23:34 fa.exe
-rwxrwxr-x 1 wsinatra wsinatra   5526 Jul 23 16:03 fa.fnl
-rwxrwxr-x 1 wsinatra wsinatra   7053 Jul 23 16:03 fennelview.fnl
-rwxrwxr-x 1 wsinatra wsinatra  15772 Jul 23 16:03 lume.lua
```